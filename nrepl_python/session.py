import uuid
import copy

sessions = {}

class Session:
    def __init__(self,id=uuid.uuid4()):
        self.id = str(id)
        self.history = []

    def push_event(self,event):
        self.history.append(event)

    def get_events(self):
        return self.history

    def last_event(self):
        return self.history[-1]

    def clone(self):
        clone = copy.deepcopy(self)
        clone.id = str(uuid.uuid4())

        return clone

    def __repr__(self):
        return f'<Session:{self.id}>'
        
    def __str__(self):
        return f'Session: ID: {self.id}, event history: \n {self.history}'
    
